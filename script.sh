#!/bin/bash
#
# Author: Peterson Yuhala
#

base="/home/petman/c-projects/pmdk-count-illegals/src/"

folders="libpmemblk,libpmemlog,libpmemobj,libpmempool,librpmem,libvmem,libvmmalloc,libpmem"

 
c_fxns="cacos,cacosf,cacosl,casin,casinf,casinl,catan,catanf,catanl,ccos,ccosf,ccosl,csin,csinf,csinl,ctan,ctanf,ctanl,
cacosh,cacoshf,cacoshl,casinh,casinhf,casinhl,catanh,catanhf,catanhl,ccosh,
ccoshf,ccoshl,csinh,csinhf,csinhl,ctanh,ctanhf,ctanhl,cexp,cexpf,cexpl,
clog,clogf,clogl,cabs,cabsf,cabsl,cpow,cpowf,cpowl,csqrt,csqrtf,csqrtl,carg,cargf,cargl,cimag,cimagf,cimagl
,conj,conjf,conjl,cproj,cprojf,cprojl,creal,crealf,creall,feclearexcept,fegetexceptflag,
feraiseexcept,fesetexceptflag,fetestexcept,fegetround,fesetround,fegetenv,feholdexcept,
fesetenv,feupdateenv,wcstoimax,wcstoumax,setlocale,
localeconv,raise,signal,remove,rename,tmpfile,tmpnam,fclose,fflush,fopen,freopen,
setbuf,setvbuf,fprintf,fscanf,printf,scanf,sprintf,sscanf,
vfprintf,vfscanf,vprintf,vscanf,vsprintf,vsscanf,fgetc,fgets,
fputc,fputs,getc,getchar,gets,putc,putchar,puts,ungetc,fread
,fwrite,fgetpos,fseek,fsetpos,rand,srand,atexit,exit,_Exit,getenv,
system,strcpy,strcat,strstr,clock,mktime,time,ctime,gmtime,localtime,fwprintf,fwscanf,swscanf,vfwprintf,vfwscanf,vswscanf,vwprintf,vwscanf,wprintf
,wscanf,fgetwc,fgetws,fputwc,fputws,fwide,getwc,getwchar,putwc,putwchar,ungetwc,wcstod,wcstof,wcstold
,wcstol,wcstoll,wcstoul,wcstoull,wcscpy,wcscat,wcsftime,wctob,iswalnum,iswalpha,iswblank,iswcntrl,
iswdigit,iswgraph,iswlower,iswprint,iswpunct,iswspace,iswupper,iswxdigit,
wctype,towlower,towupper,towctrans,wctrans" 

cpp_fxns_classes="nested_exception,timed_mutex,recursive_timed_mutex"

c_cpp_keywords_types="__transaction_atomic,__transaction_relaxed,__transaction_cancel,SCNiLEASTN,
SCNoLEASTN,SCNuLEASTN,SCNxLEASTN,
SCNdFASTN,SCNiFASTN,SCNoFASTN,
SCNuFASTN,SCNxFASTN,SCNdMAX,
SCNiMAX,SCNoMAX,sig_atomic_t,SIG_DFL,
SCNuMAX,SCNxMAX,SIG_ERR,SIG_IGN,
SCNdPTR,SCNiPTR,fpos_t,
_IOFBF,_IOLBF,_IONBF,
FILENAME_MAX,FOPEN_MAX,L_tmpnam
SCNoPTR,SCNuPTR,SCNxPTR,LC_CTYPE,
LC_MONETARY,LC_NUMERIC,
LC_TIME,struct lconv,SIGABRT,SIGFPE,SIGILL,
SIGINT,SIGSEGV,SIGTERM,TMP_MAX,stderr,
stdin,stdout,SEEK_CUR,SEEK_END,SEEK_SET,TMP_MAX"

fxn_count=0
type_count=0
class_count=0
var=0
IFS=,

for dir in $folders;
do    
    path="${base}${dir}" 
    cd "$base"   
    touch "$dir.csv"
    cd "$path"
    var=0
    fxn_count=0
    type_count=0
    class_count=0
    count=0
    
    
     
        echo "-----Unsupported C Fxns-----" >> "../$dir.csv"
        for fxn in $c_fxns
        do
            for file in *
            do
                var=$(grep -o "$fxn" "$file" | wc -l)
                count=$((count+var))                
            done         
            
            echo "${fxn},${count}" >> "../$dir.csv"            
            fxn_count=$((count+fxn_count))
            count=0
        done
            echo "Total illegal fxns: ${fxn_count}" >> "../$dir.csv"

        echo "-----Unsupported Cpp Classes-----" >> "../$dir.csv"
        for class in $cpp_fxns_classes
        do
            for file in *
            do
                var=$(grep -o "$class" "$file" | wc -l)
                count=$((count+var)) 
            done
            
            echo "${class},${count}" >> "../$dir.csv" 
            class_count=$((count+class_count))
            count=0
        done
            echo "Total illegal cpp classes: ${class_count}" >> "../$dir.csv"

        echo "-----Unsupported C/Cpp Keywords/Types-----" >> "../$dir.csv"
        for type in $c_cpp_keywords_types
        do 
            for file in *
            do
                var=$(grep -o "$type" "$file" | wc -l)
                count=$((count+var)) 

            done
            
            echo "${type},${count}" >> "../$dir.csv"
            type_count=$((count+type_count))
            count=0
        done 
            echo "Total illegal c/cpp Keywords/Types: ${type_count}" >> "../$dir.csv"
        
       
    #touch "$dir.csv"
done